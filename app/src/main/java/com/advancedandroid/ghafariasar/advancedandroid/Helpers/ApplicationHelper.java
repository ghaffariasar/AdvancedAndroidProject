package com.advancedandroid.ghafariasar.advancedandroid.Helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class ApplicationHelper {

    Context _context;



    public ApplicationHelper(Context context) {
        _context = context;
    }

    public void ShowToast(String message) {
        Toast.makeText(_context, message, Toast.LENGTH_SHORT).show();
    }

    public void alert(String title , String message){
        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.show();
    }

    public void SetCulture(String lang) {

        String languageToLoad = lang;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        _context.getApplicationContext().getResources().updateConfiguration(config, _context.getApplicationContext().getResources().getDisplayMetrics());
    }


    public float fahrenheitToCelsius(float fahrenheitValue) {
        return ((fahrenheitValue - 32) * 5) / 9;
    }

    public String fahrenheitToCelsius(String fahrenheitValue) {
        int  result = Integer.parseInt(fahrenheitValue);
        int finalResult = ((result - 32) * 5) / 9;
        return Integer.toString(finalResult);
    }

    public String getPersianWeekdayName(String weekdayName) {

        switch (weekdayName.toLowerCase()){

            case "sat":
                return "شنبه";

            case "sun":
                return "یکشنبه";

            case "mon":
                return "دو شنبه";

            case "tue":
                return "سه شنبه";

            case "wed":
                return "چهارشنبه";

            case "thu":
                return "پنج شنبه";

            case "fri":
                return "جمعه";
        }

        return "";
    }


    public static String GetDateTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)_context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
