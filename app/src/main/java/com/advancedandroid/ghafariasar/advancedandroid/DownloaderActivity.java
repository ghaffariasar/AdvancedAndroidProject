package com.advancedandroid.ghafariasar.advancedandroid;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.advancedandroid.ghafariasar.advancedandroid.CustomViews.CustomEditText;
import com.advancedandroid.ghafariasar.advancedandroid.Models.EventBusModel;
import com.advancedandroid.ghafariasar.advancedandroid.Services.DownloaderService_;
import com.rey.material.widget.ProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


@EActivity(R.layout.activity_downloader)
public class DownloaderActivity extends BaseActivity {

    private ProgressDialog progressDialog;

    @ViewById
    CustomEditText txtDownloadUrl;

    @StringRes
    String filed_input_required;


    @Click
    void btnDownload() {
        String urlValue = txtDownloadUrl.getText().toString();

        if (urlValue.isEmpty()) {
            txtDownloadUrl.setError(filed_input_required);
            return;
        }

        Intent downloaderService = new Intent(mContext, DownloaderService_.class);
        downloaderService.putExtra("url", urlValue);
        startService(downloaderService);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode != 100) {
            ShowToast("Permission Requierd...");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(mContext);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(mContext);
    }

    @AfterViews
    protected void OnViewsLoadComplete() {
        assignVariables();
        bindEvents();
        checkPermissions();
    }


    @Subscribe()
    public void onEvent(EventBusModel eventBusModel) {
        boolean isFinished = eventBusModel.isFinished();
        int progressValue = eventBusModel.getProgress();

        if (isFinished) {
            progressDialog.hide();
            progressDialog = null;
            return;
        }
        if (progressDialog == null)
            initializeProgressDialog();

        progressDialog.setProgress(progressValue);
    }

    private void initializeProgressDialog() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Downloading...");
        // progressDialog.setTitle();
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();
    }

    private void checkPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        }

    }

    private void assignVariables() {

    }

    private void bindEvents() {

        txtDownloadUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtDownloadUrl.getText().length() > 0)
                    txtDownloadUrl.clearError();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}
