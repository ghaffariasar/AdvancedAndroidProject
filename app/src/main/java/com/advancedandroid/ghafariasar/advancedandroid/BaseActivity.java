package com.advancedandroid.ghafariasar.advancedandroid;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class BaseActivity extends AppCompatActivity {

    public String TAG = getClass().getSimpleName();
    public Activity mActivity = this;
    public Context mContext = this;
    public ProgressDialog dialog;


    public void ShowToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }


    public void ShowProgressDialog(String message) {
        dialog = new ProgressDialog(mContext);
        dialog.setMessage(message);
        dialog.show();
    }

    public void HideProgressDialog( ) {
        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();
    }
}
