package com.advancedandroid.ghafariasar.advancedandroid.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.advancedandroid.ghafariasar.advancedandroid.Helpers.ApplicationHelper;
import com.advancedandroid.ghafariasar.advancedandroid.Models.EventBusModel;
import com.advancedandroid.ghafariasar.advancedandroid.Models.MovieModel;
import com.advancedandroid.ghafariasar.advancedandroid.Models.OmdbResponseModel;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.Extra;
import org.greenrobot.eventbus.EventBus;

import java.io.File;

import cz.msebera.android.httpclient.Header;


@EService
public class DownloaderService extends Service {

    public String TAG = "DownloaderService";
Context mContext;



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent.hasExtra("url")){
            String url  = intent.getStringExtra("url");
            download(url);
        }
        return START_STICKY;
    }

    private void download(String url){
        AsyncHttpClient httpClient = new AsyncHttpClient();

        httpClient.get(url, new FileAsyncHttpResponseHandler(getApplicationContext()) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Log.d(TAG , "error in download");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                Toast.makeText(DownloaderService.this, "file has been downloaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);

                float value = (float) bytesWritten / (float)totalSize;
                int finalDownloadedPercent = Math.round(value * 100);
                Log.d(TAG , "download : " + finalDownloadedPercent);

                EventBus.getDefault().post(new EventBusModel(finalDownloadedPercent));
            }

            @Override
            public void onFinish() {
                super.onFinish();
                EventBus.getDefault().post(new EventBusModel(true));
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
