package com.advancedandroid.ghafariasar.advancedandroid;


import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;

import com.advancedandroid.ghafariasar.advancedandroid.CustomViews.CustomButton;
import com.advancedandroid.ghafariasar.advancedandroid.CustomViews.CustomEditText;
import com.advancedandroid.ghafariasar.advancedandroid.CustomViews.CustomTextView;
import com.advancedandroid.ghafariasar.advancedandroid.Helpers.ApplicationHelper;
import com.advancedandroid.ghafariasar.advancedandroid.Models.MovieModel;
import com.advancedandroid.ghafariasar.advancedandroid.Models.OmdbResponseModel;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import cz.msebera.android.httpclient.Header;


@EActivity(R.layout.activity_movie_info)
public class MovieInfoActivity extends BaseActivity {

    ApplicationHelper applicationHelper;

    @ViewById
    CustomEditText txtMovieTitle;

    @ViewById
    CustomButton btnGetMovieInfo;

    @ViewById
    ImageView imagePoster;

    @ViewById
    CustomTextView lblTitle;

    @ViewById
    CustomTextView lblYear;

    @ViewById
    CustomTextView lblDirector;

    @ViewById
    CustomTextView lblGenre;

    @ViewById
    TableLayout tblMovieInfo;

    @StringRes
    String pleaseWait;

    @StringRes
    String errorNetworkConnection;

    @StringRes
    String errorInRecieve;

    @StringRes
    String resultIsEmpty;

    @StringRes
    String filed_input_required;




    @AfterViews
    void OnViewsLoadComplete() {
        assignVariables();
        bindEvents();
        clearElements();
    }

    @Click
    void btnGetMovieInfo() {

        loadMovieInformation();

    }


    private void loadMovieInformation() {
        if (!applicationHelper.isNetworkAvailable()) {
            ShowToast(errorNetworkConnection);
            return;
        }

        String movieTitle = txtMovieTitle.getText().toString();

        if (movieTitle.isEmpty()) {
            txtMovieTitle.setError(filed_input_required);
            return;
        }
        txtMovieTitle.clearError();

        ShowProgressDialog(pleaseWait);
        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.get("http://www.omdbapi.com/?t=" + movieTitle, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                ShowToast(errorInRecieve);
                clearElements();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                Gson gson = new Gson();

                OmdbResponseModel omdbResponseModel = gson.fromJson(responseString, OmdbResponseModel.class);

                if (omdbResponseModel.getResponse().toString().equals("False")) {
                    ShowToast(resultIsEmpty);
                    clearElements();
                    return;
                }

                tblMovieInfo.setVisibility(View.VISIBLE);

                MovieModel movieModel = gson.fromJson(responseString, MovieModel.class);
                lblTitle.setText(movieModel.getTitle());
                lblYear.setText(movieModel.getYear());
                lblGenre.setText(movieModel.getGenre());
                lblDirector.setText(movieModel.getDirector());

                Glide.with(mContext).load(movieModel.getPoster()).centerCrop().crossFade().into(imagePoster);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                HideProgressDialog();
            }
        });
    }

    private void assignVariables() {
        if (applicationHelper == null)
            applicationHelper = new ApplicationHelper(this);
    }

    private void bindEvents() {

        txtMovieTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtMovieTitle.getText().length() > 0)
                    txtMovieTitle.clearError();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    private void clearElements() {
        tblMovieInfo.setVisibility(View.INVISIBLE);
        imagePoster.setImageResource(0);
        txtMovieTitle.selectAll();
    }
}
