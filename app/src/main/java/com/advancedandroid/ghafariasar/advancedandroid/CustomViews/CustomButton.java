package com.advancedandroid.ghafariasar.advancedandroid.CustomViews;

import android.content.Context;
import android.util.AttributeSet;

import com.advancedandroid.ghafariasar.advancedandroid.Application.MainApplication;
import com.rey.material.widget.Button;



public class CustomButton extends Button {


    public CustomButton(Context context) {
        super(context);

        this.setTypeface(MainApplication.IranSansTypeface);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(MainApplication.IranSansTypeface);

    }
}
