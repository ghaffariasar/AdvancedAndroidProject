package com.advancedandroid.ghafariasar.advancedandroid.Application;

import android.app.Application;
import android.graphics.Typeface;


public class MainApplication extends Application {

    public static Typeface IranSansTypeface ;

    public MainApplication() {
        super();
    }

    private void setTypefaces() {
        if(IranSansTypeface == null)
            IranSansTypeface = Typeface.createFromAsset(getApplicationContext().getAssets() , "sans.ttf");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setTypefaces();


    }
}
