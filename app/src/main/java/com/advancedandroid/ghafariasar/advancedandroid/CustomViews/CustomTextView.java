package com.advancedandroid.ghafariasar.advancedandroid.CustomViews;

import android.content.Context;
import android.util.AttributeSet;

import com.advancedandroid.ghafariasar.advancedandroid.Application.MainApplication;
import com.rey.material.widget.TextView;





public class CustomTextView extends TextView {


    public CustomTextView(Context context) {
        super(context);

        this.setTypeface(MainApplication.IranSansTypeface);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(MainApplication.IranSansTypeface);
    }


}
