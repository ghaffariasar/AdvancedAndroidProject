package com.advancedandroid.ghafariasar.advancedandroid.Models;



public class EventBusModel {

    int progress = 0;
    boolean isFinished = false;


    public EventBusModel(int progressValue) {
        progress = progressValue;
    }

    public EventBusModel(boolean isFinished) {
        this.isFinished = isFinished;
    }

    public int getProgress() {
        return progress;
    }


    public boolean isFinished() {
        return this.isFinished;
    }
}
