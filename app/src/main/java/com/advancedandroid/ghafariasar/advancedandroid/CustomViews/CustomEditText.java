package com.advancedandroid.ghafariasar.advancedandroid.CustomViews;

import android.content.Context;
import android.util.AttributeSet;

import com.advancedandroid.ghafariasar.advancedandroid.Application.MainApplication;
import com.rey.material.widget.EditText;



public class CustomEditText extends EditText {


    public CustomEditText(Context context) {
        super(context);
        this.setTypeface(MainApplication.IranSansTypeface);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(MainApplication.IranSansTypeface);
    }
}
